package com.tencent.wxcloudrun.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dto.CounterRequest;
import com.tencent.wxcloudrun.model.Counter;
import com.tencent.wxcloudrun.service.CounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.List;

/**
 * counter控制器
 */
@RestController

public class CounterController {

  final CounterService counterService;
  final Logger logger;

  public CounterController(@Autowired CounterService counterService) {
    this.counterService = counterService;
    this.logger = LoggerFactory.getLogger(CounterController.class);
  }


  /**
   * 提交鉴定
   * @return API response json
   */
  @PostMapping(value = "/api/insertCounter")
  ApiResponse insertCounter(@RequestBody CounterRequest request) {
    logger.info("提交鉴定");
    int i ;
    try {
      i = counterService.insertCounter(request);
    }catch (Exception e){
      return ApiResponse.error("提交失败");
    }
    if (i == 1){
      return ApiResponse.ok("提交成功");
    }else {
      return ApiResponse.ok("提交失败");
    }

  }

  /**
   * 获取获取详情
   * @return API response json
   */
  @PostMapping(value = "/api/getCounter")
  ApiResponse getCounter(String id) {
    logger.info("获取详情");
    Optional<Counter> counter = counterService.getCounter(Integer.valueOf(id));
    return ApiResponse.ok(counter);
  }

  /**
   * 审核
   * @return API response json
   */
  @GetMapping(value = "/api/updateExamine")
  ApiResponse updateExamine(String examine,String id) {
    logger.info("审核鉴定");
    int i ;
    try {
      i = counterService.updateExamine(examine, Integer.valueOf(id));
    }catch (Exception e){
      return ApiResponse.error("处理成功");
    }
    if (i == 1){
      return ApiResponse.ok("处理成功");
    }else {
      return ApiResponse.ok("处理失败");
    }

  }

  /**
   * 更新计数，自增或者清零
   * @param request {@link CounterRequest}
   * @return API response json

  @PostMapping(value = "/api/count")
  ApiResponse create(@RequestBody CounterRequest request) {
    logger.info("/api/count post request, action: {}", request.getAction());

    Optional<Counter> curCounter = counterService.getCounter(1);
    if (request.getAction().equals("inc")) {
      Integer count = 1;
      if (curCounter.isPresent()) {
        count += curCounter.get().getCount();
      }
      Counter counter = new Counter();
      counter.setId(1);
      counter.setCount(count);
      counterService.upsertCount(counter);
      return ApiResponse.ok(count);
    } else if (request.getAction().equals("clear")) {
      if (!curCounter.isPresent()) {
        return ApiResponse.ok(0);
      }
      counterService.clearCount(1);
      return ApiResponse.ok(0);
    } else {
      return ApiResponse.error("参数action错误");
    }
  }
   */
  
}