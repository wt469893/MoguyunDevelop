package com.tencent.wxcloudrun.service;

import com.tencent.wxcloudrun.dto.CounterRequest;
import com.tencent.wxcloudrun.model.Counter;

import java.util.Optional;
import java.util.List;

public interface CounterService {

  Optional<Counter> getCounter(Integer id);

  void clearCount(Integer id);

  int insertCounter(CounterRequest request);

  int updateExamine(String examine,Integer id);
}
