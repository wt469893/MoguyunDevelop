package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Counter implements Serializable {

  private Integer id;

  //采集姓名
  private String name;
  //采集人手机号
  private String phone;
  //图片
  private String pic;
  //采集描述
  private String gatherDes;
  //采集地点
  private String gatherAddress;
  //快递单号
  private String trackingNum;
  //鉴定地址
  private String address;
  //鉴定状态
  private Integer examine;

  private LocalDateTime createdAt;

  private LocalDateTime updatedAt;
}
