package com.tencent.wxcloudrun.dao;

import com.tencent.wxcloudrun.dto.CounterRequest;
import com.tencent.wxcloudrun.model.Counter;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

@Mapper
public interface CountersMapper {

  Counter getCounter(@Param("id") Integer id);

  void clearCount(@Param("id") Integer id);

  int insertCounter(@Param("request")CounterRequest request);

  int updateExamine(@Param("examine")String examine,@Param("id")Integer id);
}
